package com.biom4st3r.overheadhp.mixin;

import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.LivingEntityRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;

import com.biom4st3r.overheadhp.OverHeadHPMod;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(EntityRenderer.class)
public class OverHeadHPMixin {

    @Shadow
    @Final
    protected EntityRenderDispatcher renderManager;

    @Inject(at = @At("RETURN"),method = "render",cancellable = false)
    public void render(Entity e, double x, double y, double z, float eYaw, float ePitch, CallbackInfo ci)
    {
        if(e instanceof LivingEntity)
        {
            LivingEntity le = (LivingEntity)e;
            double distanceFromCamera = le.squaredDistanceTo(renderManager.camera.getPos());
            if(distanceFromCamera <= 64)
            {
                boolean isSneaking = le.isInSneakingPose();
                float playerYaw = renderManager.cameraYaw;
                float playerPitch = renderManager.cameraPitch;
                float labelHeight = le.getHeight() + 0.75F - (isSneaking ? 0.25F : 0.0F);
                GameRenderer.renderFloatingText(
                    renderManager.getTextRenderer(),
                     String.format("%.02f / %.02f", le.getHealth(), le.getHealthMaximum()), 
                     (float)x, (float)(y+labelHeight), (float)z, 0, playerYaw, playerPitch, isSneaking); //0 for not dinerbone
            }
        }

    }

}
